FROM mhart/alpine-node:12

WORKDIR /app

ADD . ./
RUN ls
RUN yarn

ENTRYPOINT ["yarn", "run", "start"]
